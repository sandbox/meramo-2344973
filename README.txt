CodeMirror Wysiwyg Bridge

-- SUMMARY --

This module acts as a bridge between the CodeMirror plugin and the Wysiwyg API
module.

-- REQUIREMENTS --

* Wysiwyg API


-- INSTALLATION --
Adapted from http://www.fmath.info/plugins/drupal/doc.jsp

Steps:
1. Download the latest dev build of the WYSIWYG module, unzip.
  Copy the folder WYSIWYG to sites/all/modules/.
  Download CKEditor from ckeditor.com/download, unzip.
  Copy CKEditor to sites/all/libraries/ckeditor/.

  Enable WYSIWYG module in Drupal
  Enable CKEditor in the WYSIWYG profiles for the formats you want it for (e.g. Filtered and Full HTML)
  Verify the CKEditor is working when you add/edit content in Drupal;

3. Install the CodeMirror WYSIWIG module:
  Add this module to Drupal and activate it.

4. Go to the WYSIWYG profile page, edit the profile and enable the "Syntax highlighting" plugin

5. Test
  Clear the cache of browser (temporary files);
  Create an equation and save;

6. Go to Source mode in ckeditor area


-- USAGE --

* Please consult the documentation of CodeMirror for further information.  This is
  just an integration module.


